**Version**: 1.1.0

**Date**: 21/02/08

**Performed by**: Gabriel Fedel

**Test setup**: Running the modules with a real equipment, and the IOC on a Virtual machine


IOC Test (LAN version)
========

The following tests are conducted using manual CA commands from promt. This test is for the LAN version of the protocol.

| Test case            | Result | Comment                                                                 |
| -------------------- | ------ | ----------------------------------------------------------------------- |
| Basic connection     | OK  |              |
| CAGet on Test       | PASS |              |
| CAGet on Vol-R       | 0.005 |              |
| CAGet on Cur-R       | 0 |              |
| CAGet on Idn-R       | GEN10-500-LAN |              |
| CAGet on ISW-R     | 2U5K:5.1.3-LAN:3.1.3.2 |              |
| CAGet on SN-R    | S/N:123B299-0001 |              |
| CAGet on Rmt-RB | LLO |             |
| CAPut on Rmt-S | LOC |             |
| CAGet on Rmt-RB | LOC |             |
| CAPut Vol-S to 10V  | OK  |   |
| CAGet on Vol-RB       | 10 |              |
| CAGet on Vol-R       | 0 | Should turn to ON to have the value applied   |
| CAPut Out-S to ON | OK |  |
| CAGet on Vol-R       | 10 |    |
| CAPut Cur-S to 1 A | OK |  |
| CAGet on Cur-RB       | 1  |              |
| CAGet on Cur-R       | 0 | Expected             |
| CAPut Rst to 1 | OK | The power goes to off and the voltage rb to 0 |
| CAPut Out-S to OFF | OK |  |
| CAGet Out-RB | OFF |  |
| CAPut Out-S to ON | OK  |  |
| CAPut FldProt-S to ON | OK |  |
| CAget FldProt-RB  | ON |  |
| CAPut OvrVolProt-S to 9V | OK |  |
| CAGet OvrVolProt-RB | 9 |  |
| CAPut OvrVolMax-S to 11V |OK |  |
| CAGet OvrVolProt-RB | 12 |  |
| CAPut UndVolLim-S to 3V | |  |
| CAGet UndVolLim-RB | OK |  |
| CAPut AutRes-S to ON | OK |  |
| CAGet AutRes-RB | ON |  |
| CAPut Rst to 1 | ON | |
| CAGet Date-R  |  | It is not avaiable on LAN version |
| CAPut ErrClr 1 | OK  |  |
| CAPut ErrSystEn-S 1 | OK  |  |
| CAGet Err-R | 0  |  |
| CAGet NxtErr-R | 0  |  |
| CAGet Error | No error  |  |
| CAGet MDAV-RB |   | It is not avaiable on LAN version |
| CAGet FldRst |   | It is not avaiable on LAN version |
| CAGet FldDly-S |   | It is not avaiable on LAN version |
| CAGet FldRst |   | It is not avaiable on LAN version |
